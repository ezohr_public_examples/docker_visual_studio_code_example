# https://docs.docker.com/engine/reference/builder/

ARG DOCKER_CONTENT_TRUST=1

FROM fedora:latest as initialImage
ARG DOCKER_CONTENT_TRUST
USER 0:0
WORKDIR /root
# ENV PASSWORD=password

FROM initialImage as upgradedImage
ARG DOCKER_CONTENT_TRUST
USER 0:0
WORKDIR /root
RUN dnf -y update
RUN dnf -y upgrade

FROM upgradedImage as basePackagesImage
ARG DOCKER_CONTENT_TRUST
USER 0:0
WORKDIR /root
RUN dnf -y install zsh

FROM basePackagesImage as dockremapImage
ARG DOCKER_CONTENT_TRUST
USER 0:0
WORKDIR /root
RUN useradd -s /usr/bin/zsh -u 10000 dockremap

FROM dockremapImage as updatedRepositoriesImage
ARG DOCKER_CONTENT_TRUST
USER 0:0
WORKDIR /root
RUN rpm --import https://packages.microsoft.com/keys/microsoft.asc
RUN zsh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
RUN dnf -y update

FROM updatedRepositoriesImage as applicationImage
ARG DOCKER_CONTENT_TRUST
USER 0:0
WORKDIR /root
RUN dnf -y install code

FROM applicationImage as applicationSupportImage
ARG DOCKER_CONTENT_TRUST
USER 0:0
WORKDIR /root

# FROM applicationSupportImage as presentationSupportImage-x11docker
# ARG DOCKER_CONTENT_TRUST
# USER 0:0
# WORKDIR /root
# # x11docker (https://github.com/mviereck/x11docker#installation-options)
# RUN curl -fsSL https://raw.githubusercontent.com/mviereck/x11docker/master/x11docker | bash -s -- --update
# # --xpra-xwayland (https://github.com/mviereck/x11docker/wiki/X-server-and-Wayland-Options#description-of-wayland-options)
# RUN dnf -y install xpra
# RUN dnf -y install weston
# RUN dnf -y install xorg-x11-server-Xwayland
# RUN dnf -y install xdotool
# # HTML 5 (https://github.com/mviereck/x11docker/wiki/Container-applications-running-in-Browser-with-HTML5)
# RUN dnf -y install python3-websockify

# FROM applicationSupportImage as presentationSupportImage-Wayland
# ARG DOCKER_CONTENT_TRUST
# USER 0:0
# WORKDIR /root
# # Getting Started With Xpra (https://xpra.org/trac/wiki/Start)
# RUN dnf -y install xpra
# # OpenGL (https://www.xpra.org/trac/wiki/Usage/OpenGL)
# RUN dnf -y install weston
# RUN dnf -y install xorg-x11-server-Xwayland
# RUN dnf -y install xdotool

FROM applicationSupportImage as presentationSupportImage-X11
ARG DOCKER_CONTENT_TRUST
USER 0:0
WORKDIR /root
# Getting Started With Xpra (https://xpra.org/trac/wiki/Start)
RUN dnf -y install xpra
# Xdummy (https://www.xpra.org/trac/wiki/Xdummy)
# Already Installed
# RUN dnf -y install xorg-x11-drv-dummy

# FROM presentationSupportImage-x11docker as presentationConfigurationImage-x11docker
# ARG DOCKER_CONTENT_TRUST
# USER 10000:10000
# WORKDIR /home/dockremap

# FROM presentationSupportImage-Wayland as presentationConfigurationImage-Wayland
# ARG DOCKER_CONTENT_TRUST
# # root
# USER 0:0
# WORKDIR /root
# # weston-launch
# RUN usermod -a -G weston-launch dockremap
# RUN chmod +s /usr/bin/weston-launch
# # dockremap
# USER 10000:10000
# WORKDIR /home/dockremap
# # ~/.Start/Start.sh
# RUN mkdir -p ~/.Start
# COPY .Start/Start-Wayland.sh ~/.Start/Start.sh
# RUN chmod +x ~/.Start/Start.sh

FROM presentationSupportImage-X11 as presentationConfigurationImage-X11
ARG DOCKER_CONTENT_TRUST
# root
USER 0:0
WORKDIR /root
# Xdummy (https://www.xpra.org/trac/wiki/Xdummy)
# RUN echo "xvfb=Xorg -dpi 96 -noreset -nolisten tcp +extension GLX +extension RANDR +extension RENDER -logfile \"\${HOME}\"/.Xpra/Xvfb-10.log -config \"\${HOME}\"/.Xpra/xorg.conf" >> /etc/xpra/xpra.conf
# RUN echo "-dpi 96 -noreset -nolisten tcp +extension GLX +extension RANDR +extension RENDER -logfile \"\${HOME}\"/.Xpra/Xvfb-10.log -config \"\${HOME}\"/.Xpra/xorg.conf" >> /etc/xpra/xpra.conf
# dockremap
USER 10000:10000
WORKDIR /home/dockremap
# ~/.Xpra/xorg.conf
# RUN mkdir -p ~/.Xpra
# COPY .Xpra/xorg.conf /home/dockremap/.Xpra/xorg.conf
# ~/.Start/Start.sh
RUN mkdir -p ~/.Start
COPY .Start/Start-X11.sh /home/dockremap/.Start/Start.sh

FROM presentationConfigurationImage-X11 as presentationImage
ARG DOCKER_CONTENT_TRUST
USER 10000:10000
WORKDIR /home/dockremap
CMD source ~/.Start/Start.sh /usr/bin/code && /usr/bin/tail -f /dev/null

FROM presentationImage as cleanImage
ARG DOCKER_CONTENT_TRUST
USER 0:0
WORKDIR /root
RUN dnf -y clean all

FROM cleanImage as production
ARG DOCKER_CONTENT_TRUST
USER 10000:10000
WORKDIR /home/dockremap