#!/usr/bin/env zsh

# ********* Dates *********

# Created: 12 July 2020

# ********* Application *********

# ========= Check Arguments =========

success=0
errorArguments=1
errorFile=2

filename="${1}"

if [ "${#}" -ne "1" ]; then
	echo
	echo "Error:	1 Argument Expected"
	echo "	Execute:	" "${0}" "[Path/Filename]"
	echo

	exit ${errorArguments}
fi

if [ ! -f "${filename}" ]; then
	echo
	echo "Error:	File" "${filename}" "Does Not Exist"
	echo

	exit ${errorFile}
fi

# ========= XDG_RUNTIME_DIR (https://wayland.freedesktop.org/building.html) =========

if test -z "${XDG_RUNTIME_DIR}"; then
  export XDG_RUNTIME_DIR=/tmp/${UID}-runtime-dir

  if ! test -d "${XDG_RUNTIME_DIR}"; then
    mkdir "${XDG_RUNTIME_DIR}"
    chmod 0700 "${XDG_RUNTIME_DIR}"
  fi
fi

# ========= HTML 5 Without Encryption Or Authentication (https://www.xpra.org/trac/wiki/Clients/HTML5) =========

/usr/bin/xpra start --bind-tcp=0.0.0.0:10000 --start="${filename}" --dpi=96

# ========= Exit =========

echo

# exit ${success}